import sys

file = sys.argv[1]

print("Truncating the file")
with open(file,"w") as f_obj:
    f_obj.truncate()

file_obj = open(file,mode="a+")
#writitng to the file
line1 = input("Enter line 1 :")
line2 = input("Enter line 2 :")
line3 = input("Enter line 3 :")

file_obj.write(line1+"\n"+line2+"\n"+line3)
file_obj.close()

with open(file,mode="r") as f_obj:
    print(f_obj.read())
