import sys

a = float(sys.argv[1])
op = sys.argv[2]
b = float(sys.argv[3])

if op == "+":
    print(a+b)
elif op == "-":
    print(a-b)
elif op == "*":
    print(a*b)
elif op == "/":
    print(a/b)
elif op == "%":
    print(a%b)
else:
    print("Operator not supported")


